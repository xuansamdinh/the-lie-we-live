# The Lie We Live - Sống Đời Vô Minh

Đây là một video mình thấy mọi người nên xem và suy ngẫm.

Video nói về con người, xã hội, thế giới chúng ta đã, đang và sẽ sống. Về ngôi nhà của chúng ta, 'anh em' của chúng ta trên ngôi nhà nhỏ bé ấy giữa bao la vũ trụ vô định này. Phải chăng chúng ta là giống loài quá ích kỷ, hung hăng, khát máu. Và u mê chừng nào.

Dưới đây là link video trên Youtube - kèm sub.

- [https://www.youtube.com/embed/ipe6CMvW0Dg][1]

Và bản dịch vụng về của mình về nội dung của video này. Thật vui nếu các bạn ghé thăm post này, và có thể hiểu ngay nguyên văn, vì bản dịch của mình chắc chắn đang hạn chế nhiều điểm. Khó lột tả được nội dung đầy trắc ẩn mà người làm video trên muốn nói.

Bản pdf tải về tại mục [Downloads][2] của kho này. A5, fontsize 11pt.

[1]: https://www.youtube.com/embed/ipe6CMvW0Dg
[2]: https://bitbucket.org/xuansamdinh/the-lie-we-live/downloads
